public class Persona{

    //Atributos
    private String nombre;
    private String apellido;
    private String dni;
    private String direccion; 
    private String telefono; 
    private String fecha_bancaria; 

    //Metodos Setters

    public void SetNombre(String nombre){
        this.nombre = nombre; 
    }

    public void SetApellido(String apellido){
        this.apellido = apellido;
    }

    public void SetDNI(String dni){
        this.dni = dni; 
    }

    public void SetDireccion(String direccion){
        this.direccion = direccion;
    }

    public void SetTelefono(String telefono){
        this.telefono = telefono; 
    }

    public void SetFechaBancaria(String fecha_bancaria){
        this.fecha_bancaria = fecha_bancaria; 
    }

    //Metodo Getters

    public String GetNombre(){
        return "El nombre es: "+nombre; 
    }

    public String GetApellido(){
        return "EL apellido es: "+apellido; 
    }

    public String GetDNI(){
        return "El DNI es: "+dni; 
    }

    public String GetDireccion(){
        return "La direccion es: "+direccion;
    }

    public String GetTelefono(){
        return "El numero de telefono: "+telefono; 
    }

    public String GetFecha_bancaria(){
        return "La fecha bancaria: "+fecha_bancaria; 
    }
}